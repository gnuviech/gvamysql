#!/bin/sh

set -e

QUEUE=mysql
TASKS=${QUEUE}tasks
APP=gvamysql
export TZ="Europe/Berlin"

. "/srv/${APP}/.venv/bin/activate"
cd /srv/${APP}/${APP}
celery -A "${TASKS}" worker -Q "${QUEUE}" -E -l info
