# -*- coding: utf-8 -*-
# pymode:lint_ignore=E501
"""
Common settings and globals.

"""

from os import environ


def get_env_setting(setting):
    """
    Get the environment setting or return exception.

    :param str setting: name of an environment setting
    :raises ImproperlyConfigured: if the environment setting is not defined
    :return: environment setting value
    :rtype: str
    """
    try:
        return environ[setting]
    except KeyError:
        error_msg = "Set the %s env variable" % setting
        raise AssertionError(error_msg)


########## CELERY CONFIGURATION
accept_content = ["json"]
broker_url = get_env_setting("GVAMYSQL_BROKER_URL")
enable_utc = True
result_backend = get_env_setting("GVAMYSQL_RESULTS_REDIS_URL")
result_expires = None
result_persistent = True
result_serializer = "json"
task_routes = ("gvacommon.celeryrouters.GvaRouter",)
task_serializer = "json"
timezone = "Europe/Berlin"
########## END CELERY CONFIGURATION

########## GVAMYSQL CONFIGURATION
GVAMYSQL_DBADMIN_HOST = get_env_setting("GVAMYSQL_DBADMIN_HOST")
GVAMYSQL_DBADMIN_PORT = int(get_env_setting("GVAMYSQL_DBADMIN_PORT"))
GVAMYSQL_DBADMIN_USER = get_env_setting("GVAMYSQL_DBADMIN_USER")
GVAMYSQL_DBADMIN_PASSWORD = get_env_setting("GVAMYSQL_DBADMIN_PASSWORD")
########## END GVAMYSQL CONFIGURATION
