"""
This module defines Celery_ tasks to manage MySQL users and databases.

"""
from celery import shared_task
from celery.utils.log import get_task_logger

from mysqltasks import settings
from MySQLdb import connect


_LOGGER = get_task_logger(__name__)


def _get_connection():
    return connect(
        host=settings.GVAMYSQL_DBADMIN_HOST,
        port=settings.GVAMYSQL_DBADMIN_PORT,
        user=settings.GVAMYSQL_DBADMIN_USER,
        passwd=settings.GVAMYSQL_DBADMIN_PASSWORD,
        db='mysql',
    )


@shared_task
def create_mysql_user(username, password):
    """
    This task creates a new MySQL user.

    :param str username: the user name
    :param str password: the password
    :return: the created user's name
    :rtype: str

    """
    conn = _get_connection()
    curs = conn.cursor()
    curs.execute(
        """
        CREATE USER %(username)s@'%%' IDENTIFIED BY %(password)s
        """,
        {'username': username, 'password': password}
    )
    conn.commit()
    return username


@shared_task
def set_mysql_userpassword(username, password):
    """
    Set a new password for an existing MySQL user.

    :param str username: the user name
    :param str password: the password
    :return: True if the password could be set, False otherwise
    :rtype: boolean

    """
    conn = _get_connection()
    curs = conn.cursor()
    curs.execute(
        """
        SET PASSWORD FOR %(username)s = PASSWORD(%(password)s)
        """,
        {'username': username, 'password': password})
    conn.commit()
    return True


@shared_task
def delete_mysql_user(username):
    """
    This task deletes an existing MySQL user.

    :param str username: the user name
    :return: True if the user has been deleted, False otherwise
    :rtype: boolean

    """
    conn = _get_connection()
    curs = conn.cursor()
    curs.execute(
        """
        DROP USER %(username)s@'%%'
        """,
        {'username': username})
    conn.commit()
    return True


@shared_task
def create_mysql_database(dbname, username):
    """
    This task creates a new MySQL database for the given MySQL user.

    :param str dbname: database name
    :param str username: the user name of an existing MySQL user
    :return: the database name
    :rtype: str

    """
    conn = _get_connection()
    curs = conn.cursor()
    curs.execute(
        """
        CREATE DATABASE `%(dbname)s` CHARACTER SET utf8 COLLATE utf8_general_ci
        """ % {'dbname': dbname})
    curs.execute(
        """
        GRANT ALL PRIVILEGES ON `%(dbname)s`.* TO %%(username)s@'%%%%'
        """ % {'dbname': dbname}, {'username': username})
    conn.commit()
    return dbname


@shared_task
def delete_mysql_database(dbname, username):
    """
    This task deletes an existing MySQL database and revokes privileges of the
    given user on that database.

    :param str dbname: database name
    :param str username: the user name of an existing MySQL user
    :return: True if the database has been deleted, False otherwise
    :rtype: boolean

    """
    conn = _get_connection()
    curs = conn.cursor()
    curs.execute(
        """
        REVOKE ALL PRIVILEGES ON `%(dbname)s`.* FROM %%(username)s@'%%%%'
        """ % {'dbname': dbname}, {'username': username})
    curs.execute(
        """
        DROP DATABASE `%(dbname)s`
        """ % {'dbname': dbname})
    conn.commit()
    return True
