"""
This module contains :py:mod:`mysqltasks.tasks`.
"""

__version__ = "0.3.0"

from mysqltasks.celery import app as celery_app

__all__ = ("celery_app",)
