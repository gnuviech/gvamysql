"""
This module defines the Celery_ app for gvamysql.

.. _Celery: http://www.celeryproject.org/

"""
from __future__ import absolute_import

from celery import Celery

#: The Celery application
app = Celery("mysqltasks")

app.config_from_object("mysqltasks.settings", namespace="CELERY")
app.autodiscover_tasks(["mysqltasks.tasks"], force=True)
