==================
Code documentation
==================

gvamysql is implemented as `Celery`_ app.

.. _Celery: http://www.celeryproject.org/


:py:mod:`mysqltasks` app
========================

.. automodule:: mysqltasks

:py:mod:`mysqltasks.celery`
---------------------------

.. automodule:: mysqltasks.celery
   :members:


:py:mod:`mysqltasks.settings`
-----------------------------

.. automodule:: mysqltasks.settings


:py:mod:`mysqltasks.tasks`
--------------------------

.. automodule:: mysqltasks.tasks
   :members:
   :undoc-members:
