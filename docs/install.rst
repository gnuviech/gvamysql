.. index:: installation

=======
Install
=======

Working Environment
===================

To get a running work environment use `pipenv`_.

.. _pipenv: https://pipenv.kennethreitz.org/en/latest/

To get started install `pip` and `pipenv` and use `pipenv install --dev`:

.. code-block:: sh

   $ apt install python3-pip
   $ python3 -m pip install --user -U pipenv
   $ pipenv install --dev

.. index:: celery, worker, file queue

Running the Celery worker
=========================

gvamysql uses the `Celery`_ distributed task queue system. The gvamysql logic is
executed by a celery worker. After all dependencies are installed you can go
into the gvamysql directory and run the celery worker with:

.. code-block:: sh

    $ cd gvamysql
    $ pipenv run celery -A gvamysql worker -Q mysql -l info

.. _Celery: http://www.celeryproject.org/
