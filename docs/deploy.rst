Deploy
========

The production deployment for gvamysql is performed using saltstack and consists
of the following steps:

* installation of native dependencies
* setup of a virtualenv
* installation of gvamysql production dependencies inside the virtualenv
* setup of celery worker under control of supervisord
