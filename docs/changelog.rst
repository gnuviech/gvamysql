Changelog
=========

* :release:`0.3.0 <2023-05-08>`
* :support:`-` switch from Pipenv to Poetry

* :release:`0.2.0 <2020-04-10>`
* :support:`-` add Docker setup for lightweight local testing
* :support:`-` update Vagrant setup to libvirt and Debian Buster
* :support:`-` move mysqltasks to top level to keep the task names when
  using Python 3
* :support:`-` drop old requirements files

* :release:`0.1.2 <2020-02-29>`
* :bug:`-` Update dependencies for Debian Buster compatibility
* :support:`-` Add `Pipfile` and `Pipfile.lock` for more modern dependency
  management

* :release:`0.1.1 <2019-03-22>`
* :bug:`-` Remove fixed kombu version due to incompatibility with current
  Python 2.7, update amqp to 1.4.9

* :release:`0.1.0 <2015-01-04>`
* :feature:`-` initial project setup
